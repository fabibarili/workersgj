﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "New NarrativeCase", menuName = "New NarrativeCase")]
public class NarrativeCase : ScriptableObject
{
    [SerializeField]
    public string descriptionCasePT;
    
    [SerializeField]
    public string descriptionCaseEU;

    [SerializeField]
    public bool alternatives;

    [SerializeField]
    public bool moneyLoss;

    [SerializeField]
    public NarrativeCase nextCase;

    [SerializeField]
    public NarrativeCase acceptCase;

    [SerializeField]
    public NarrativeCase declineCase;

    [SerializeField]
    public Sprite BackgroundImage;

    [SerializeField]
    public int scoreAccept;

    [SerializeField]
    public int scoreDecline;

    [SerializeField]
    public int moneyWin;

    [SerializeField]
    public int moneyLose;
    
    [SerializeField]
    public int moneyLossExtra;
}
