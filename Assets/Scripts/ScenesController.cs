﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesController : MonoBehaviour
{
    public void SceneGameEN()
    {
        SceneManager.LoadScene("GameEN");
    }

    public void SceneGameBR()
    {
        SceneManager.LoadScene("GameBR");
    }
}
