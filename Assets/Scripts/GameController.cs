﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameController : MonoBehaviour
{
    public GameObject nextCaseButton;
    public GameObject acceptCaseButton;
    public GameObject declineCaseButton;
    public TextMeshProUGUI descriptionCaseTextPT;
    public TextMeshProUGUI descriptionCaseTextEU;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI moneyText;
    public NarrativeCase nodesNarrative;
    private NarrativeCase _nextCase;
    private NarrativeCase _currentNarrative;
    private NarrativeCase _oldNarrative;
    private NarrativeCase _currentAcceptNarrative;
    private NarrativeCase _currentDeclineNarrative;
    private int _currentScorePlayer;
    private int scorePlayer;
    private int scoreAccept;
    private int scoreDecline;
    private int moneyWin;
    private int moneyLoss;
    private int moneyPrejuizo;
    private int _moneyPlayer;
    public Image background;
    
    void Start (){
        scorePlayer = 0;
        descriptionCaseTextPT.text = nodesNarrative.descriptionCasePT.ToString();
        descriptionCaseTextEU.text = nodesNarrative.descriptionCaseEU.ToString();

        moneyText.text = "Dinheiro: " + _moneyPlayer.ToString();
        scoreText.text = "Pontos: " + scorePlayer.ToString();
        
        firstCase();
    }

    void FixedUpdate(){
        background.sprite = _currentNarrative.BackgroundImage;
        //_currentScorePlayer = _currentNarrative.scoreAccept;
        
        scoreAccept = _currentNarrative.scoreAccept;
        scoreDecline = _currentNarrative.scoreDecline;
        
        nodeType();
    }

    //identificar se o step da narrativa tem alternativas ou não
    public void nodeType(){
        if(_currentNarrative.alternatives == true){
            acceptCaseButton.SetActive(true);
            declineCaseButton.SetActive(true);
            nextCaseButton.SetActive(false);
        } else {
            acceptCaseButton.SetActive(false);
            declineCaseButton.SetActive(false);
            nextCaseButton.SetActive(true);
        }

        if(_currentNarrative.moneyLoss == true){
            
        }

    }

    public void UpdateMoney(){

    }

    //coloca os primeiros steps da narrativa nas variáveis corretas
    public void firstCase(){
        _currentNarrative = nodesNarrative;
        _nextCase = _currentNarrative.nextCase;
        nodeType();
    }

    //faz a atualização dos steps para passar para os próximos
    public void uploadCase(){

        _currentNarrative = _nextCase;
        _nextCase = _currentNarrative.nextCase;

        _currentAcceptNarrative = _currentNarrative.acceptCase;
        _currentDeclineNarrative = _currentNarrative.declineCase;

        //Debug.Log("ATUAL " + _currentNarrative.name + " NEXT " + _nextCase.name + " ACEITO " + _currentAcceptNarrative.name);
    }

    //passar para o próximo step simples da narrativa (com uma só opção "next")
    public void nextStep(){
        uploadCase();
        descriptionCaseTextPT.text = _currentNarrative.descriptionCasePT.ToString();
        descriptionCaseTextEU.text = _currentNarrative.descriptionCaseEU.ToString();
    }

    public void UploadeScoreAccept(){
        _currentScorePlayer = scoreAccept;
        scorePlayer += _currentScorePlayer;
        _currentScorePlayer = scorePlayer;
        scoreText.text = "Pontos: " + _currentScorePlayer.ToString();
    }

    public void UploadeScoreDecline(){
        _currentScorePlayer = scoreDecline;
        scorePlayer -= _currentScorePlayer;
        _currentScorePlayer = scorePlayer;
        scoreText.text = "Pontos: " + _currentScorePlayer.ToString();
    }

    //atualiza e exibe o step quando é aceito o caso
    public void acceptCase(){
        _currentNarrative = _currentAcceptNarrative;
        _nextCase = _currentNarrative.nextCase;
        _currentAcceptNarrative = _currentNarrative.acceptCase;
        _currentDeclineNarrative = _currentNarrative.declineCase;

        descriptionCaseTextPT.text = _currentNarrative.descriptionCasePT.ToString();
        descriptionCaseTextEU.text = _currentNarrative.descriptionCaseEU.ToString();

        UploadeScoreAccept();

    }

    //atualiza e exibe o step quando é recusado o caso
    public void declineCase(){
        _currentNarrative = _currentDeclineNarrative;
        _nextCase = _currentNarrative.nextCase;
        _currentAcceptNarrative = _currentNarrative.acceptCase;
        _currentDeclineNarrative = _currentNarrative.declineCase;

        descriptionCaseTextPT.text = _currentNarrative.descriptionCasePT.ToString();
        descriptionCaseTextEU.text = _currentNarrative.descriptionCaseEU.ToString();


        UploadeScoreDecline();
    }
     
}
